num = int(input())
part3 = (num % 1) * 10

if part3 >= 5:
    print(int((num // 1) + 1))
elif part3 >= 5 or (num // 1) <= 10:
    print(int(num // 1))
else:
    print(int(num // 1))
